# permissions.yml

## About

This configuration file contains the permissions for the Outpost plugin.

## Configuration Vales

**settings.op-all-perms**

* **default**: `true`
* **options**: `true`, `false`
* **description**: If oped players have permissions to run all outpost commands.
___

**permissions.command.outpost**

* **default**: `outpost.outpost`
* **description**: The permission to do the command `/outpost`.
___

**permissions.command.help**

* **default**: `outpost.help`
* **description**: The permission to do the command `/outpost help`.
___

**permissions.command.admin.help**

* **default**: `outpost.admin.help`
* **description**: The permission to do the command `/outpost help` and display the admin help page.
___

**permissions.command.admin.reload**

* **default**: `outpost.admin.reload`
* **description**: The permission to do the command `/outpost reload`.
___

**permissions.command.admin.list**

* **default**: `outpost.admin.list`
* **description**: The permission to do the command `/outpost list`.
___

**permissions.command.admin.setarea**

* **default**: `outpost.admin.setarea`
* **description**: The permission to do the command `/outpost setarea <outpost>`.
___

**permissions.command.admin.create**

* **default**: `outpost.admin.create`
* **description**: The permission to do the command `/outpost create <outpost>`.
___