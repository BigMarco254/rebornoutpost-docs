# config.yml

## About

This configuration file contains the global settings for the Outpost plugin.

## Configuration Vales

**thirdparty.factions-versions**

* **default**: `auto`
* **options**: `auto`, `1.6`, `1.8`, `2.6`, `2.8.6`
* **note**: The `.`'s (periods) can be replaced with `_`'s (underscores) if you please.
* **description**: Used to determine which factions API to use. Setting to `auto` will let the plugin decide which factions API your factions plugin is using.
___

**timer-type**

* **default**: `thread-async`
* **options**: `thread-async`, `bukkit-async`, `bukkit-sync`
* **note**: The `-`'s (dashes) can be replaced with `_`'s (underscores) if you please. `thread-async` will create its own thread. `bukkit-async` and `bukkit-sync` will use the built in bukkit schedulers.
* **description**: The type of scheduler used to run every second.
___

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.